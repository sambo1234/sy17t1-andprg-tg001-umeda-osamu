﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PipeSpawner : MonoBehaviour
{
    public GameObject originalPipe;

    public float PipePos;
    public float xMove;
    public Text score;
    public scr Player;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("SpawnPipe", 2.0f, 1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        Score();
    }
    void SpawnPipe()
    {
        GameObject clone = Instantiate(originalPipe, new Vector3(PipePos, Random.Range(0, -6.3f), 0), Quaternion.identity);
        PipePos = xMove;
        xMove += 10;
        Destroy(clone, 5.0f);
    }
    void Score()
    {
        score.text = Player.playerScore.ToString();
    }
}
