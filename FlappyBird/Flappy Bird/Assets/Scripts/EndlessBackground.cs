﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessBackground : MonoBehaviour {
   
    public GameObject originalBG;
    public GameObject originalFloor;

    public float xBackground;
    public float yBackground;
    public float xFloor;
    public float yFloor;
    public float xMove;

	// Use this for initialization
	void Start () {
        InvokeRepeating("SpawnBG", 2.0f, 1.0f);

	}
	
	// Update is called once per frame
	void Update () {

    }
    void SpawnBG()
    {
        GameObject clone = Instantiate(originalBG, new Vector3(xBackground, yBackground, 0), Quaternion.identity);
        GameObject cloneTwo = Instantiate(originalFloor, new Vector3(xFloor, yFloor, 0), Quaternion.identity);
        xBackground = xMove;
        xFloor = xMove;
        xMove += 10;
        Destroy(clone, 5.0f);
        Destroy(cloneTwo, 5.0f);
    }
}
