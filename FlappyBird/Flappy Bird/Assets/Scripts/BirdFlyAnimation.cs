﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdFlyAnimation : MonoBehaviour {

    private Animator birdFlap;
	// Use this for initialization
	void Start ()
    {
        birdFlap = GetComponent<Animator>();
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            birdFlap.Play("BirdFly");
        }
        else if(Input.GetKeyUp(KeyCode.Space))
        {
            birdFlap.Play("fly");
        }
                    
	}
}
