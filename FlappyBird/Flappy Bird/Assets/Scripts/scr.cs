﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr : MonoBehaviour {
    public float JumpForce;
    public float forward;
    public  float fall;

    public int playerScore;

    public bool isDead;

    public bool isStart;

    private Rigidbody2D rb;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space) && !isDead)
        {
            if(!isStart)
            {
                isStart = true;
                rb.gravityScale = 2.3f;  
            }
            rb.velocity = new Vector2(forward, JumpForce);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "GameOverZone")
        {
            isDead = true;
        }
        if (other.tag == "ScoreZone")
        {
            playerScore++;
        }

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "GameOverZone")
        {
            isDead = true;
        }
        if (other.transform.tag == "ScoreZone")
        {
            playerScore++;
        }
    }
}