﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform player;
    float offsetX;

	// Use this for initialization
	void Start () {
        player = player.transform;

        offsetX = transform.position.x - player.position.x;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = transform.position;
        pos.x = player.position.x + offsetX;
        transform.position = pos;
	}
}
