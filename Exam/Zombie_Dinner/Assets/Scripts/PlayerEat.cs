﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerEat : MonoBehaviour {
    public string Tag;
    public string TagTwo;
    public string TagThree;
    public string powerUpTag;
    public string powerUpTagTwo;

    public Text playerScore;
    public GameObject gameOverScreen;

    public int score;
    public int targetScore = 5;

    public float scaleIncrease;
    public float playerScale = 2;
    public float megaIvanovScale;
    public float maxSize;
    public float growFactor;
    public float waitTime;


    public float dupeOneXSpawn;
    public float dupeOneYSpawn;
    public float dupeTwoXSpawn;
    public float dupeThreeXSpawn;
    public float dupeFourXSpawn;
    public float dupeFiveXSpawn;
    public float restartDelay;

    float restartTimer;

    public bool isDead;

    public GameObject smallZombie;
    public GameObject medZombie;
    public GameObject largeZombie;
    public GameObject dupeOne;
    public GameObject dupeTwo;
    public GameObject dupeThree;
    public GameObject dupeFour;
    public GameObject dupeFive;
    // Use this for initialization
    void Start () {
        gameOverScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        StartCoroutine(Pause());
        Score();
    }   

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == Tag)
        {
            if (playerScale > SmallZombie.smallZScale)
            {


                score++;
                Destroy(other.gameObject);
                if (score >= targetScore)
                {
                    targetScore += 5;
                    transform.localScale += new Vector3(scaleIncrease, scaleIncrease);
                    playerScale += 2;
                }
            }
            else if (playerScale < SmallZombie.smallZScale)
            {
                isDead = true;
            }
        }
        else if(other.transform.tag == TagTwo)
        {
            if (playerScale > MediumZombie.medZScale)
            {
                score++;
                Destroy(other.gameObject);
                if (score >= targetScore)
                {
                    targetScore += 5;
                    transform.localScale += new Vector3(scaleIncrease, scaleIncrease);

                }
            }
            else if (playerScale < MediumZombie.medZScale)
            {
                isDead = true;
            }

        }
        else if(other.transform.tag == TagThree)
        {
            if (playerScale > LargeZombie.LargeZScale)
            {
                score++;
                Destroy(other.gameObject);
                if (score >= targetScore)
                {
                    targetScore += 5;
                    transform.localScale += new Vector3(scaleIncrease, scaleIncrease);

                }
            }
            else if (playerScale < LargeZombie.LargeZScale)
            {
                isDead = true;
            }
        }

        else  if(other.transform.tag == powerUpTagTwo)
        {
            StartCoroutine(MegaIvanovPU(other));
            Destroy(other.gameObject);
        }

        else if(other.transform.tag == powerUpTag)
        {
            Invoke("DupliIvanov", 1.0f);
            Destroy(other.gameObject);
        }

    }

    IEnumerator MegaIvanovPU(Collision2D other)
    {
        float timer = 0;

        while (true)
        { 
            while (maxSize > transform.localScale.x)
            {
                timer += Time.deltaTime;
                transform.localScale += new Vector3(1, 1, 1) * Time.deltaTime * growFactor;
                playerScale += Time.deltaTime;
                yield return null;
            }
   

            yield return new WaitForSeconds(waitTime);

            timer = 0;
            while (1 < transform.localScale.x)
            {
                timer += Time.deltaTime;
                transform.localScale -= new Vector3(1, 1, 1) * Time.deltaTime * growFactor;
                playerScale -= Time.deltaTime;
                yield return null;
            }

            timer = 0;
            yield return new WaitForSeconds(waitTime);
        }
    }

    IEnumerator Pause()
    {
        if (isDead)
        {
            Time.timeScale = 0.0f;
            gameOverScreen.SetActive(true);
            PlayerControls.speed -= 1;
            yield return new WaitForSecondsRealtime(5.0f);
            Time.timeScale = 1;
            SceneManager.LoadScene("Zombie_Dinner");
            PlayerControls.speed = 1;
        }
    }
    void DupliIvanov()
    {
        GameObject clone = Instantiate(dupeOne, new Vector3(dupeOneXSpawn, dupeOneYSpawn, 0), Quaternion.identity);
        GameObject cloneTwo = Instantiate(dupeTwo, new Vector3(dupeTwoXSpawn, dupeOneYSpawn, 0), Quaternion.identity);
        GameObject cloneThree = Instantiate(dupeThree, new Vector3(dupeThreeXSpawn, dupeOneYSpawn, 0), Quaternion.identity);
        GameObject cloneFour = Instantiate(dupeFour, new Vector3(dupeFourXSpawn, dupeOneYSpawn, 0), Quaternion.identity);
        GameObject cloneFive = Instantiate(dupeFive, new Vector3(dupeFiveXSpawn, dupeOneYSpawn, 0), Quaternion.identity);

        Destroy(clone, 5.0f);
        Destroy(cloneTwo, 5.0f);
        Destroy(cloneThree, 5.0f);
        Destroy(cloneFour, 5.0f);
        Destroy(cloneFive, 5.0f);
    }

    void Score()
    {
        playerScore.text = "score: " + score.ToString();
    }
}
