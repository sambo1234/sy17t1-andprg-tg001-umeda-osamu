﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float MinX, MaxX, MinY, MaxY;
}
public class PlayerControls : MonoBehaviour {
    public Boundary boundary;
    public static float speed = 1;

    private Vector3 screenPoint;
    private Vector3 offset;
    private bool mouseDown;

    void Start()
    {
        mouseDown = false;
    }

    // Update is called once per frame

    void OnMouseDown()
    {
        mouseDown = true;
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    void OnMouseDrag()
    {
        Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
       

        cursorPosition.x = Mathf.Clamp(cursorPosition.x, boundary.MinX, boundary.MaxX);
        cursorPosition.y = Mathf.Clamp(cursorPosition.y, boundary.MinY, boundary.MaxY);


        transform.position = Vector3.Lerp(transform.position, cursorPosition, speed);
    }

    void OnMouseUp()
    {
        mouseDown = false;
    }
}
