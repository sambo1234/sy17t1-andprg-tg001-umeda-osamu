﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour {
    public GameObject powerUpGO;

    public float startSpawnSec;
    public float spawnRateInSecs;
    void Start()
    {
        InvokeRepeating("SpawnZombie", startSpawnSec, spawnRateInSecs);

    }

    void SpawnZombie()
    {
        Vector3 min = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 max = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 1));

        GameObject anEnemy = (GameObject)Instantiate(powerUpGO);
        anEnemy.transform.position = new Vector3(Random.Range(min.x, max.x), max.y, max.z);
    }
}
