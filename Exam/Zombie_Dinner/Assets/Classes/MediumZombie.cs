﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MediumZombie : Zombie
{
    public static int medZScale = 3;

    protected override void Move()
    {
        base.Move();
        speed = 3.0f;
    }
}
