﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LargeZombie : Zombie
{
    public static int LargeZScale = 5;
    protected override void Move()
    {
        base.Move();
        speed = 4.0f;
    }

}